library ieee;
use ieee.std_logic_1164.all;
USE IEEE.NUMERIC_STD.ALL;

entity somador is
	port(
		--signal clk: in std_logic;
		--signal reset: in std_logic;
		--signal clk_en: in std_logic;
		--signal start: in std_logic;
		--signal done: out std_logic;
		--signal n: in std_logic_vector(7 downto 0);
		dataa: in std_logic_vector(31 downto 0);
		datab: in std_logic_vector(31 downto 0);
		--signal a: in std_logic_vector(4 downto 0);
		--signal b: in std_logic_vector(4 downto 0);
		--signal c: in std_logic_vector(4 downto 0);
		--signal readra: in std_logic;
		--signal readrb: in std_logic;
		--signal writerc: in std_logic;
		result: out std_logic_vector(31 downto 0)
	);
end entity somador;

architecture b_custominstruction of somador is

BEGIN
	--comparing : process(clk)
	--begin
		--if (rising_edge(clk)) then
			result <= std_logic_vector(signed(dataa(31 downto 0)) + signed(datab(31 downto 0)));
		--end if;
	--end process comparing;
end architecture b_custominstruction;